angular.module('iframeAutoHeight')

  .directive('iframeAutoHeight', function ($interval) {
    var stepSize = 100, // Alto del step
        stepInterval = 200, // Intervalo para control del alto
        stepSizeMax = stepSize * 2;

    return {
      restrict: 'C',
      link: function (scope, element, attrs) {
        var iframe = element[0],
            iahi, h;

        scope.start = function () {
          if (!angular.isDefined(iahi)) {
            iahi = $interval(function () {
              if (iframe.contentWindow.document.body) { // Wait for body ready
                // Nota: La reducción de tamaño inicial, es para no dejar un margen en la parte inferior luego,
                // pero tiene alto consumo de CPU si el intervalo es muy corto
                h = iframe.contentWindow.document.body.scrollHeight;
                iframe.style.height = ((h > stepSizeMax) ? (h - stepSize) : stepSize) + "px"; // Remueve exedente del alto
                iframe.style.height = iframe.contentWindow.document.body.scrollHeight + "px"; // Alto sin scroll
              }
            }, stepInterval);
          }
        };

        scope.stop = function () {
          if (angular.isDefined(iahi)) {
            $interval.cancel(iahi);
            iahi = undefined;
          }
        };

        scope.$on('$destroy', function () {
          // Make sure that the interval is destroyed too
          scope.stop();
        });

        scope.start();
      }
    }
  });